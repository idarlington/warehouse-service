# Postgres Data Warehouse

For this assignment, you will create a data warehouse based on a single Postgres instance. The source data for your warehouse are published on two Kafka topics.

## Domain
The data that you will ingest comes from a contrived example of transactions on a digital asset (crypto) exchange. There are two entities that we are conserned with: *user*, and *transaction*.

### Users
A user has a handful of properties that typically remain static over time, but could (infrequently) change, such as a user's bank account details, or email address.

#### Churn
Users may churn on the platform, meaning that a user ceases their activity and is not likely to return. Users rarely actually delete their accounts, so to detect a churning user, we have to look at users who dump their positions, and almost fully withdraw their funds from their account (leaving perhaps a minimal amount). A user can be said to have churned when their account funds are mostly withdrawn, and no further activity occurs for the user.

#### Trading Volume
The value of a user is mostly determined by their total trading volume (on which exchanges earn commission). A user's trading volume is the sum of the value in Euro of all *trade* transactions that they perform.

### Transactions
Transactions occur when a user deposits funds into their account, withdraws funds from their account, or executes an order on the exchange (the orders themselves, including matching, and filling are out of scope of this assignment). As such, a user transaction can be of type *deposit*, *withdrawal*, or *trade*. A number of additional fields are (optionally) present that describe the details of the transaction.

# Requirements
The data warehouse upon successful ingestion is required to serve two use cases:
1. Basic reporting on user behaviour
2. Insight into trending assets (trending crypto currencies)

## Basic Reporting
For the first point, the following basic insights must be easy to query from the database schema that you design:
- Who are the top N users by trading volume on day X?
- Who are the top N churning users by trading volume on day X?
- What is the overall trading volume by day / week / month?
- What is the account balance for each individual user on day X?

For each of these, keep in mind that we need to be able to query this for both the latest state, as well as any day in the past.

## Trending Assets
For this requirement, you will build a pipeline that populates a history of trending assets for each day. Trending is not necessarily the same as popularity. An asset for which the trading volume has a strong upwards trend, does not need to be the most popular asset based on absolute volume.

The exact definition of trending is not provided. Part of the exercise is to provide your own definition of trending, whic his both *concise*, and *mathematically precise*. This will help creating a explainable pipeline.

The trending assets pipeline is expected to be streaming such that it adds the latest trending asset information immediately after the rollover of a time window. It should have a configurable time window (so you can configure it to create streaming assets for the past day, or for the past five minute interval).

## Non-functional
When building your solution, also consider non-functional reasonable requirements, such as:
- being able to backfill older data if necessary
- refreshing data warehouse contents after pipeline changes
- scheduling, observability, and alerting

Although the above might not be fully implemented as part of the exercise (because of time contraints), your design decisions, and other considerations are expected to show that you've thought about these things.

# Obtaining the Data
This assignment comes with a single `docker-compose.yml` file that will build up the required basic infrastructure as well as the data producer for this assignment. A single `docker compose up` command should suffice to get going.

## Kafka
New user creation or user update events are published on a topic called `user`. Transaction events are published on a topic called `transaction`. Messages use Apache Avro as serialization format. The message schemas are registered with the Confluent Schema Registry, and can be inspected at run time.

The Kafka cluster that is spun up from the docker compose file is a full fledged Confluent cluster. This includes Schema Registry, and Confluent Control Center. You can inspect the running cluster by navigating to [http://localhost:9021/clusters](the confluent web UI on localhost). To inspect both the messages as well as the schema for the Kafka topics, you can use this web UI.

Note that the provided Confluent cluster also has all the facilities to create stateful consumers (ksqlDB) or deploy Kafka Connect, in case you care to use these.

## Postgres
As part of the docker compose, a Postgres instance will be spun up that you can connect to:
- hostname: localhost
- username: bitvavo
- password: bitvavo

If you have the Postgres client CLI installed, you should be able to connect using: `psql -h 127.0.0.1 -U bitvavo` (you will be prompted for the password).

Since docker compose will spin a volatile setup, restarting will leave you with an empty database. This means that you will have to recreate any schema on / after startup.

## Troubleshooting
If the initial docker compose setup does not work out of the box for you, please be in touch. It is not the goal of this assignment to spend time troubleshooting issues with the provided installation. Just ask.

# Goals and Evaluation
The goal of this assignment is to evaluate your skills in the area of data engineering, which we believe consists of:
- a strong foundation in software engineering
- mastering data modelling techniques
- building robust pipelines
- familiarity with common data issues (quality, broken records, etc.)

In your solution we hope to be convinced of your expertise in this area. At the same time, we appreciate the fact that a (practically) time boxed exercise will leave room for further improvements in the area of productionizing, and polishing. Not that your solution is the starting point of an evaluation session, and concerns that were not addressed in the solution itself, can be topic of conversation at that time.

An explicit non-goal of this exercise is to spend time messing with infrastructure. If you use tech like dbt, Airflow / Dagster, or standalone Kafka consumer applications, just run them locally on your dev machine from a default installation. You should not feel the need to spend time setting up Docker images, creating deployment strategies, or otherwise invest time in a single startup command, super reproducible setup.

# Deliverable
Please provide (a downloadable) archive of your solution ahead of time before the scheduled evaluation session. Short notice (day before, same they in the morning) is not a problem. The evaluation session always takes place; regardless of our assessment of the solution / code itself. Our hiring decisions are based on both the delivered result, as well as the session in which we discuss the solution, and design.
