package com.idarlington.stream.processors

import com.idarlington.stream.TestData
import com.idarlington.stream.model.UserAggregateMessage
import com.idarlington.stream.processors.Fixture.{inputTopic, withDriverIO}
import org.apache.kafka.streams.test.TestRecord
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class TransactionsProcessorSpec extends AnyWordSpec with Matchers {

  "TransactionsProcessor" should {
    "process deposits" in withDriverIO { (input, output) =>
      val records = TestData.deposits
        .map(new TestRecord(inputTopic, _))

      records.foreach(input.pipeInput)

      val result = output.readKeyValuesToMap()

      result.get(TestData.userGUID1).get shouldBe
      UserAggregateMessage(
        user_guid        = TestData.userGUID1,
        last_activity_ts = "2023-11-04T12:33:36.980069Z",
        withdrawals_eur  = 0,
        deposits_eur     = 12500,
        trades_eur       = 0,
        balance_eur      = 12500,
        maybe_churning   = false
      )

      result.get(TestData.userGUID2).get shouldBe
      UserAggregateMessage(
        user_guid        = TestData.userGUID2,
        last_activity_ts = "2023-11-04T12:48:24.199273Z",
        withdrawals_eur  = 0,
        deposits_eur     = 11000,
        trades_eur       = 0,
        balance_eur      = 11000,
        maybe_churning   = false
      )

    }

    "process trades" in withDriverIO { (input, output) =>
      val records = (TestData.deposits ++ TestData.trades)
        .map(new TestRecord(inputTopic, _))

      records.foreach(input.pipeInput)

      val result = output.readKeyValuesToMap()

      result.get(TestData.userGUID1).get shouldBe
      UserAggregateMessage(
        user_guid        = TestData.userGUID1,
        last_activity_ts = "2023-11-05T10:08:43.338892Z",
        withdrawals_eur  = 0,
        deposits_eur     = 12500,
        trades_eur       = -12000,
        balance_eur      = 500,
        maybe_churning   = false
      )

      result.get(TestData.userGUID2).get shouldBe
      UserAggregateMessage(
        user_guid        = TestData.userGUID2,
        last_activity_ts = "2023-11-05T10:09:07.006178Z",
        withdrawals_eur  = 0,
        deposits_eur     = 11000,
        trades_eur       = -7245.54,
        balance_eur      = 3754.46,
        maybe_churning   = false
      )
    }

    "process withdrawals and quantifies churning user" in withDriverIO { (input, output) =>
      val records = (TestData.deposits ++ TestData.trades ++ TestData.withdrawals)
        .map(new TestRecord(inputTopic, _))

      records.foreach(input.pipeInput)

      val result = output.readKeyValuesToMap()

      result.get(TestData.userGUID1).get shouldBe
      UserAggregateMessage(
        user_guid        = TestData.userGUID1,
        last_activity_ts = "2023-11-06T11:52:11.697359Z",
        withdrawals_eur  = 450,
        deposits_eur     = 12500,
        trades_eur       = -12000,
        balance_eur      = 50,
        maybe_churning   = true
      )

      result.get(TestData.userGUID2).get shouldBe
      UserAggregateMessage(
        user_guid        = TestData.userGUID2,
        last_activity_ts = "2023-11-05T12:16:14.764739Z",
        withdrawals_eur  = 100,
        deposits_eur     = 11000,
        trades_eur       = -7245.54,
        balance_eur      = 3654.46,
        maybe_churning   = false
      )

      TestData.deposits
        .filter(_.user_guid == TestData.userGUID1)
        .map(new TestRecord(inputTopic, _))
        .foreach(input.pipeInput)

      output
        .readKeyValuesToMap()
        .get(TestData.userGUID1)
        .get
        .maybe_churning shouldBe false
    }
  }

}
