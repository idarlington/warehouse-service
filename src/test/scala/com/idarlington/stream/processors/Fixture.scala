package com.idarlington.stream.processors

import com.idarlington.stream.StreamSettings
import com.idarlington.stream.model.{RawTransaction, UserAggregateMessage}
import com.idarlington.stream.serialization.AvroSerde._
import com.idarlington.stream.serialization.RecordFormats._
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient
import io.confluent.kafka.schemaregistry.testutil.MockSchemaRegistry
import org.apache.kafka.common.serialization.{Deserializer, Serializer}
import org.apache.kafka.streams.scala.serialization.Serdes
import org.apache.kafka.streams.{StreamsConfig, TestInputTopic, TestOutputTopic, TopologyTestDriver}

import java.util.Properties
import scala.language.higherKinds

object Fixture {
  val inputTopic: String  = StreamSettings.inputTopic
  val outputTopic: String = StreamSettings.outputTopic

  val props = new Properties()
  props.put(StreamsConfig.APPLICATION_ID_CONFIG, StreamSettings.appID)
  props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, StreamSettings.bootstrapServers)

  def withDriverIO(
    testCode: (
      TestInputTopic[String, RawTransaction],
      TestOutputTopic[String, Option[UserAggregateMessage]]
    ) => Any
  ): Unit = {
    implicit val schemaRegistryClient: SchemaRegistryClient =
      MockSchemaRegistry.getClientForScope("testing-data")

    val aggregator         = new TransactionsAggregator()
    val transactionsStream = new TransactionsProcessor(aggregator).topology()

    val testDriver: TopologyTestDriver =
      new TopologyTestDriver(transactionsStream.build(), props)

    val input: TestInputTopic[String, RawTransaction] = testDriver
      .createInputTopic(
        inputTopic,
        Serdes.stringSerde.serializer,
        implicitly[Serializer[RawTransaction]]
      )

    val output: TestOutputTopic[String, Option[UserAggregateMessage]] = testDriver
      .createOutputTopic(
        outputTopic,
        Serdes.stringSerde.deserializer,
        implicitly[Deserializer[Option[UserAggregateMessage]]],
      )

    try {
      testCode(input, output)
    } finally {
      testDriver.close()
      schemaRegistryClient.reset()
    }
  }

}
