package com.idarlington.stream.processors

import com.idarlington.stream.StreamSettings
import com.idarlington.stream.model.{RawTransaction, Transaction, UserAggregate, UserAggregateMessage}
import com.idarlington.stream.serialization.AvroSerde._
import com.idarlington.stream.serialization.RecordFormats._
import com.idarlington.stream.utils.TimeUtils._
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient
import io.scalaland.chimney.dsl._
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.streams.StreamsConfig
import org.apache.kafka.streams.scala.ImplicitConversions._
import org.apache.kafka.streams.scala._
import org.apache.kafka.streams.scala.kstream.KStream

import java.util.Properties

class TransactionsProcessor(aggregator: TransactionsAggregator)(
  implicit schemaRegistryClient: SchemaRegistryClient
) {
  import org.apache.kafka.streams.scala.serialization.Serdes._

  val config = new Properties()
  config.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, StreamSettings.autoResetConfig)
  config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, StreamSettings.bootstrapServers)
  config.put(StreamsConfig.APPLICATION_ID_CONFIG, StreamSettings.appID)
  config.put("schema.registry.url", StreamSettings.schemaRegistryURL)

  def topology(): StreamsBuilder = {
    val builder: StreamsBuilder = new StreamsBuilder
    val rawTransactionStream: KStream[String, RawTransaction] =
      builder.stream[String, RawTransaction](StreamSettings.inputTopic)

    val transformed = rawTransactionStream.map { (topic, rawTransaction) =>
      (
        topic,
        rawTransaction
          .into[Transaction]
          .withFieldComputed(_.time, rt => parseZDT(rt.timestamp))
          .transform
      )
    }

    transformed
      .groupBy((_, transactionWithDate) => transactionWithDate.user_guid)
      .aggregate(UserAggregate())(
        (key, twd, state) => aggregator.apply(key, twd, state)
      )
      .toStream
      .map { (topic, userAgg) =>
        (
          topic,
          UserAggregateMessage(
            user_guid        = userAgg.userGUID,
            last_activity_ts = formatZDT(userAgg.lastActivityDate),
            withdrawals_eur  = userAgg.withdrawals,
            deposits_eur     = userAgg.deposits,
            trades_eur       = userAgg.trades,
            balance_eur      = userAgg.balance,
            maybe_churning   = userAgg.maybeChurning
          )
        )
      }
      .to(StreamSettings.outputTopic)

    builder
  }

}
