package com.idarlington.stream.processors

import com.idarlington.stream.model._
import com.idarlington.stream.utils.TimeUtils
import org.apache.kafka.streams.kstream.Aggregator

class TransactionsAggregator() extends Aggregator[String, Transaction, UserAggregate] {

  override def apply(
    key: String,
    value: Transaction,
    userAgg: UserAggregate
  ): UserAggregate = {
    val updatedUserAgg = value.transaction_type.toUpperCase match {
      case "TRADE" =>
        userAgg
          .copy(trades = userAgg.trades + value.amount_eur)
      case "DEPOSIT" =>
        userAgg.copy(deposits = userAgg.deposits + value.amount_eur)
      case "WITHDRAWAL" =>
        val sumWithdrawals = (userAgg.withdrawals + value.amount_eur)
        userAgg.copy(withdrawals = sumWithdrawals)
    }

    val churning =
      maybeChurning(
        currBalance    = userAgg.balance,
        sumWithdrawals = updatedUserAgg.withdrawals
      )
    val latestBalance  = (updatedUserAgg.trades + updatedUserAgg.deposits) - updatedUserAgg.withdrawals
    val latestActivity = TimeUtils.maxDate(userAgg.lastActivityDate, value.time)

    updatedUserAgg.copy(
      lastActivityDate = latestActivity,
      balance          = latestBalance,
      userGUID         = key,
      maybeChurning    = churning
    )
  }

  private def maybeChurning(currBalance: BigDecimal, sumWithdrawals: BigDecimal): Boolean = {
    if ((currBalance > BigDecimal(100))
        && (sumWithdrawals / currBalance >= BigDecimal(0.9)))
      true
    else false
  }

}
