package com.idarlington.stream

import com.typesafe.config.{ Config, ConfigFactory }

object StreamSettings {
  private val conf: Config = ConfigFactory.load()

  val bootstrapServers: String  = conf.getString("kafka-streams.bootstrap-servers")
  val appID: String             = conf.getString("kafka-streams.application-id")
  val autoResetConfig: String   = conf.getString("kafka-streams.auto-reset-config")
  val inputTopic: String        = conf.getString("kafka-streams.input-topic")
  val outputTopic: String       = conf.getString("kafka-streams.output-topic")
  val schemaRegistryURL: String = conf.getString("kafka-streams.schema-registry.url")

  val srClientCacheSize: Int =
    conf.getInt("kafka-streams.schema-registry.client-cache-size")
}
