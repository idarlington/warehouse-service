package com.idarlington.stream.utils

import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

object TimeUtils {

  def parseZDT(timeString: String): ZonedDateTime = {
    val pattern           = "yyyy-MM-dd'T'HH:mm:ss[.SSSSSSS][.SSSSSS][.SSSSS][.SSSS][.SSS][.SS][.S]XXX";
    val dateTimeFormatter = DateTimeFormatter.ofPattern(pattern)
    ZonedDateTime.parse(timeString, dateTimeFormatter)
  }

  def formatZDT(time: ZonedDateTime): String = {
    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSSXXX")
    time.format(formatter)
  }

  def maxDate(currDate: ZonedDateTime, providedDate: ZonedDateTime): ZonedDateTime = {
    if (providedDate.isAfter(currDate)) {
      providedDate
    } else {
      currDate
    }
  }

}
