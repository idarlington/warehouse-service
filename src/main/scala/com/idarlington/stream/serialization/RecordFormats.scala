package com.idarlington.stream.serialization

import com.idarlington.stream.model.{RawTransaction, Transaction, UserAggregate, UserAggregateMessage}
import com.idarlington.stream.utils.TimeUtils
import com.sksamuel.avro4s._
import org.apache.avro.Schema
import org.apache.avro.Schema.Field

import java.time.ZonedDateTime
import scala.math.BigDecimal.RoundingMode

object RecordFormats {

  implicit val sp: ScaleAndPrecisionAndRoundingMode =
    ScaleAndPrecisionAndRoundingMode(
      scale        = 4,
      precision    = 19,
      roundingMode = RoundingMode.HALF_EVEN
    )

  implicit val fromRecordT: FromRecord[RawTransaction] = FromRecord[RawTransaction]
  implicit val toRecordT: ToRecord[RawTransaction]     = ToRecord[RawTransaction]

  implicit val fromRecordTWD: FromRecord[Transaction] = FromRecord[Transaction]
  implicit val toRecordTWD: ToRecord[Transaction]     = ToRecord[Transaction]

  implicit val fromRecordUAgg: FromRecord[UserAggregate] =
    FromRecord[UserAggregate]
  implicit val toRecordUAgg: ToRecord[UserAggregate] =
    ToRecord[UserAggregate]

  implicit val fromRecordUAggM: FromRecord[UserAggregateMessage] =
    FromRecord[UserAggregateMessage]
  implicit val toRecordUAggM: ToRecord[UserAggregateMessage] =
    ToRecord[UserAggregateMessage]

  implicit object ZonedDateTimeFromValue extends FromValue[ZonedDateTime] {
    override def apply(value: Any, field: Field): ZonedDateTime = TimeUtils.parseZDT(value.toString)
  }

  implicit object ZonedDateTimeToValue extends ToValue[ZonedDateTime] {
    override def apply(value: ZonedDateTime): String = TimeUtils.formatZDT(value)
  }

  implicit object ZonedDateTimeToSchema extends ToSchema[ZonedDateTime] {
    protected val schema: Schema = Schema.create(Schema.Type.STRING)
  }
}
