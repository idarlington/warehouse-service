package com.idarlington.stream.serialization

import com.ovoenergy.kafka.serialization.avro4s._
import com.ovoenergy.kafka.serialization.core._
import com.sksamuel.avro4s._
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient
import org.apache.kafka.common.serialization.{ Deserializer, Serde, Serializer }
import org.apache.kafka.streams.scala.serialization.Serdes

object AvroSerde {

  implicit def serializer[A >: Null: ToRecord: FromRecord](
    implicit schemaRegistryClient: SchemaRegistryClient
  ): Serializer[A] =
    formatSerializer(
      Format.AvroBinarySchemaId,
      avroBinarySchemaIdSerializer[A](
        schemaRegistryClient,
        isKey              = false,
        includesFormatByte = false
      )
    )

  implicit def deserializer[A >: Null: ToRecord: FromRecord](
    implicit schemaRegistryClient: SchemaRegistryClient
  ): Deserializer[Option[A]] = {
    optionalDeserializer(
      avroBinarySchemaIdDeserializer[A](
        schemaRegistryClient,
        isKey              = false,
        includesFormatByte = true
      )
    )
  }

  implicit def serde[A >: Null: ToRecord: FromRecord](
    implicit serializer: Serializer[A],
    deserializer: Deserializer[Option[A]]
  ): Serde[A] =
    Serdes.fromFn[A](
      (topic: String, data: A) => serializer.serialize(topic, data),
      (topic: String, bytes: Array[Byte]) => deserializer.deserialize(topic, bytes)
    )

}
