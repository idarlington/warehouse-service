package com.idarlington.stream.model

import java.time.{Instant, ZoneId, ZonedDateTime}

case class UserAggregate(
  userGUID: String                = "",
  lastActivityDate: ZonedDateTime = ZonedDateTime.ofInstant(Instant.EPOCH, ZoneId.systemDefault()),
  withdrawals: BigDecimal         = BigDecimal(0),
  deposits: BigDecimal            = BigDecimal(0),
  trades: BigDecimal              = BigDecimal(0),
  balance: BigDecimal             = BigDecimal(0),
  maybeChurning: Boolean          = false
)

case class UserAggregateMessage(
  user_guid: String,
  last_activity_ts: String,
  withdrawals_eur: BigDecimal,
  deposits_eur: BigDecimal,
  trades_eur: BigDecimal,
  balance_eur: BigDecimal,
  maybe_churning: Boolean
)
