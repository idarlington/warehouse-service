package com.idarlington.stream.model

import com.sksamuel.avro4s.AvroNamespace

case class RawTransaction(
  timestamp: String,
  user_guid: String,
  asset: Option[String],
  amount: Option[BigDecimal],
  amount_eur: BigDecimal,
  transaction_type: String
)


