package com.idarlington.stream.model

import java.time.ZonedDateTime

case class Transaction(
  time: ZonedDateTime,
  user_guid: String,
  asset: Option[String],
  amount_eur: BigDecimal,
  transaction_type: String
)
