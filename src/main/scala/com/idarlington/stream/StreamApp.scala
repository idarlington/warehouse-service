package com.idarlington.stream

import com.idarlington.stream.processors.{ TransactionsAggregator, TransactionsProcessor }
import io.confluent.kafka.schemaregistry.client.{ CachedSchemaRegistryClient, SchemaRegistryClient }
import org.apache.kafka.streams.KafkaStreams

import java.time.Duration
import scala.jdk.CollectionConverters._

object StreamApp extends App {
  implicit val schemaRegistryClient: SchemaRegistryClient =
    new CachedSchemaRegistryClient(
      StreamSettings.schemaRegistryURL,
      StreamSettings.srClientCacheSize,
      Map.empty[String, String].asJava
    )

  private val aggregator            = new TransactionsAggregator()
  private val transactionsProcessor = new TransactionsProcessor(aggregator)
  private val transactionsStream = new KafkaStreams(
    transactionsProcessor.topology().build(),
    transactionsProcessor.config
  )

  transactionsStream.start()

  sys.ShutdownHookThread {
    transactionsStream.close(Duration.ofSeconds(10))
  }
}
