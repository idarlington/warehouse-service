# Data Warehouse service

In this project, a data warehousing service has been implemented. It consumes data from various Kafka topics, performs
transformations and aggregations then exports the results to a Postgres database.

## Architecture

A high level overview of the implementation is shown:

![High level overview](data-warehouse.jpg)

The service consists of:

- A [`Kafka` streams application](src) that aggregates transactions for each user.
- `Kafka` connect JDBC-Sink [connectors](connectors) that exports data from specified topics to Postgres DB
- `KsqlDB` [queries](ksqldb) creating streaming functions that transform and aggregate data

The design of the service relies on tools in the Kafka ecosystem leveraging on the scalability, message guarantees and
throughput they provide. This was also motivated by the data already existing in Kafka.

### Streams application

This is a Kafka streams application built in `Scala` that aggregates transactions by `user` and creates a total of
`trades`, `deposits` and `withdrawals` made by a user. The application is able to calculates a user's `balance` and
`churning users` based on this aggregation.

### Kafka Connect connectors

This component exports data from topics into Postgres. Exported data include:

- transformed `transactions`
- anonymized `users`
- `trends` analysis
- `daily transactions` by `user` and `transaction type`
- `total user transactions`

### `KsqlDB` queries

These queries create streaming processes in `KsqlDB` transform and aggregate data. Functionalities include:

- `transformation` and `parsing` of raw transactions data
- `aggregating daily transactions` by user and transaction type
- `trends` analysis

## How to run

### Docker

Start the [docker compose](docker-compose.yml)

```shell
docker compose down -v && docker compose up -d
```

Check that the cluster is up and containers are running. Opening `localhost:9021/clusters` gives an indication of the
cluster status

### Streams application

This application is built in `Scala`, and uses `sbt` as its build tool.
Install [sbt](https://www.scala-sbt.org/download.html) preferably use [`sdkman`](https://sdkman.io/sdks#sbt).
From the root folder run

```shell
sbt compile run  # run the application
sbt test         # run the tests
```

### KsqlDB queries

The [basic queries](ksqldb/basic.ksql) are run as part of the `ksqldb-cli` container in the docker compose.

To run the trends query copy the contents of the [file](ksqldb/trends.ksql) and paste into a KsqlDB interactive
interface in the browser or through a `ksql` cli.

I couldn't get this version of KsqlDB to do
[variable substitution](https://docs.ksqldb.io/en/0.29.0-ksqldb/how-to-guides/substitute-variables/).
However, to configure the pipeline, simply change the value at `line 14`
`WINDOW TUMBLING (SIZE ~~>5 HOURS<~~ , GRACE PERIOD 0 HOURS)` to match the intended interval.

### Connectors

Run the create connectors [script](scripts/create-connectors.sh)

```shell
./scripts/create-connectors.sh
```

## Database schema

![Database schema](warehoue-schema.png)

[Sample queries](sql) for basic reporting use case have also been included.

## Design choices / assumptions

- user data in the warehouse should be anonymized (no PID data)
- a user is ~= `churning` on the day when they withdraw more than `90 %` of their `balance`; for users with
  `balance > 100 Euros`
- `balance = (sum(deposit) + sum(trades)) - sum(withdrawals)`
- an `asset` is trending based on the change in `trading volume` `(sum(trades)/ count(trades))`.
- a `dead letter queue` topic is specified for `connectors` so they don't fail at processing/exporting a single
  message, messages from this topic can be exported later.

```
trade_volume = sum(trades) / count(trades)
trend = (dx(trade_volume)/previous_trade_volume) * 100
```

- `jdbc-sink connectors` use `upsert` mode with a specified `primary key` to make sure duplicates don't introduce new
  rows but updates existing rows.

## Areas of improvement

### Design

- A `transaction_id` should be added to indicate unique transactions.
  There are no guarantees on overwriting a transaction even with the
  key (`ts`, `user_guid`, `amount_eur`, `transaction_type`) as this can be duplicated.
- Currently, duplicated data is not handled well for aggregation streams and could lead to wrong values.
  Options to handle this include:
    - Introduce a `transaction_id` as described above.
    - Add a `stream processor` before the aggregators that discards messages with already processed `transaction_id`.
    - Use `exactly once` processing configuration for the aggregators to guarantee there are not duplicates between the
      aggregator pipeline.

### Implementation

- The trends analysis is better done with a `Kafka` streams app; `KsqlDB` offers little flexibility on custom
  aggregation but is more suited for simple aggregations and transformations.
- Ideally, the trends pipeline which appears to be one of batch processing and not realtime is better done with a tool
  that supports batch processing eg `Spark`
- Naming format for topics (currently mix of upper-case, camel-case)
- Add documentation to new avro schema generated from aggregations
    - annotate in the streams app
- Testing
    - Add testing for the KsqlDB queries
    - More tests for the streams application
    - Integration tests