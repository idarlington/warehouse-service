import sbt._

resolvers += "Artifactory" at "https://kaluza.jfrog.io/artifactory/maven"
resolvers += "Confluent" at "https://packages.confluent.io/maven/"
resolvers += "Redhat" at "https://maven.repository.redhat.com/ga/"

lazy val root = (project in file(".")).settings(
  inThisBuild(
    List(
      organization := "com.idarlington",
      scalaVersion := "2.12.17"
    )
  ),
  name := "transactions-stream-app",
  libraryDependencies := Seq(
    library.kafkaClients,
    library.kafkaStreams,
    library.log4jCore,
    library.chimney,
    library.slf4j,
    library.typesafeConfig,
    library.kafkaStreamsAvro,
    library.serializationCore,
    library.serializationAvro4s,
    library.kafkaTest,
    library.scalaTest
  )
)

lazy val library = new {

  val version = new {
    val kafkaVersion        = "3.5.1"
    val kafkaAvro           = "6.2.0"
    val scalaTest           = "3.2.17"
    val log4jCore           = "2.11.1"
    val typesafeConfig      = "1.4.2"
    val kafkaSerializationV = "0.7.1"
    val chimney             = "0.8.0"
    val slf4j               = "1.7.36"
  }

  val kafkaClients = "org.apache.kafka" % "kafka-clients"        % version.kafkaVersion
  val kafkaStreams = "org.apache.kafka" %% "kafka-streams-scala" % version.kafkaVersion

  val kafkaAvro         = "io.confluent"  % "kafka-avro-serializer"     % version.kafkaAvro
  val kafkaStreamsAvro  = "io.confluent"  % "kafka-streams-avro-serde"  % version.kafkaAvro
  val serializationCore = "com.ovoenergy" %% "kafka-serialization-core" % version.kafkaSerializationV
 val serializationAvro4s = "com.ovoenergy" %% "kafka-serialization-avro4s" % version.kafkaSerializationV
  val avro4s = "com.sksamuel.avro4s" % "avro4s-core_2.12" % "4.0.12"

  val chimney        = "io.scalaland"             %% "chimney"      % version.chimney
  val typesafeConfig = "com.typesafe"             % "config"        % version.typesafeConfig
  val log4jCore      = "org.apache.logging.log4j" % "log4j-core"    % version.log4jCore
  val slf4j          = "org.slf4j"                % "slf4j-log4j12" % version.slf4j

  val kafkaTest = "org.apache.kafka" % "kafka-streams-test-utils" % version.kafkaVersion
  val scalaTest = "org.scalatest"    %% "scalatest"               % version.scalaTest % "test"
}
