-- top N users by trading volume on day X
SELECT "USER_GUID",
       "DATE",
       "TOTAL_AMOUNT" AS trading_volume
FROM daily_user_transactions dut
WHERE dut."TRANSACTION_TYPE" = 'TRADE'
  AND DATE("DATE") = '<X>'
ORDER BY abs("TOTAL_AMOUNT") DESC
LIMIT <N>;

-- top N churning users on day X
WITH distinct_users AS
  (SELECT DISTINCT ON (user_guid) user_guid,
                      last_activity_ts,
                      withdrawals_eur,
                      trades_eur,
                      maybe_churning,
                      balance_eur
   FROM total_user_transactions tut
   WHERE date(last_activity_ts) = '<X>'
     AND maybe_churning = TRUE
   ORDER BY user_guid,
            last_activity_ts DESC)
SELECT *
FROM distinct_users
ORDER BY abs(distinct_users.trades_eur) DESC
LIMIT <N>;


--overall trading volume by day
SELECT DATE(t."TS") AS DAY,
       SUM(t."AMOUNT_EUR") AS trading_volume
FROM transactions t
WHERE t."TRANSACTION_TYPE" = 'TRADE'
GROUP BY DAY;

-- overall trading volume by week
SELECT DATE_TRUNC('week', t."TS") AS WEEK,
       SUM(t."AMOUNT_EUR") AS trading_volume
FROM transactions t
WHERE t."TRANSACTION_TYPE" = 'TRADE'
GROUP BY WEEK;

-- overall trading volume by month
SELECT DATE_TRUNC('month', t."TS") AS MONTH,
       SUM(t."AMOUNT_EUR") AS trading_volume
FROM transactions t
WHERE t"TRANSACTION_TYPE" = 'TRADE'
GROUP BY MONTH;

-- user balance on day x
SELECT user_guid,
       balance_eur
FROM total_user_transactions t
WHERE user_guid = '<user_guid>'
  AND date(last_activity_ts) <= '<X>'
ORDER BY t.last_activity_ts DESC ;