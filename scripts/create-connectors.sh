#!/bin/sh

printf "\n\n⏳ Waiting for Kafka connect to be available before adding connectors \n"
until curl -s -f -o /dev/null "http://localhost:8083/"; do
  echo "Connect server:  (waiting for 200)"
  sleep 5
done
printf "\n\n🔉 Adding connectors \n\n"

curl -X PUT localhost:8083/connectors/users-jdbc-sink/config \
  -H "Content-Type: application/json" -d @connectors/users-jdbc-sink.json | jq

curl -X PUT localhost:8083/connectors/transactions-jdbc-sink/config \
  -H "Content-Type: application/json" -d @connectors/transactions-jdbc-sink.json | jq

curl -X PUT localhost:8083/connectors/total-user-transactions-jdbc-sink/config \
  -H "Content-Type: application/json" -d @connectors/total-user-transactions-jdbc-sink.json | jq

curl -X PUT localhost:8083/connectors/trends-jdbc-sink/config \
  -H "Content-Type: application/json" -d @connectors/trends-jdbc-sink.json | jq

curl -X PUT localhost:8083/connectors/daily-user-transactions-jdbc-sink/config \
  -H "Content-Type: application/json" -d @connectors/daily-user-transactions-jdbc-sink.json | jq

printf "\n\n🟩 Done… \n\n"
